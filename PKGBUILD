# Maintainer: Carl Smedstad <carsme@archlinux.org>
# Contributor: Kyle Keen <keenerd@gmail.com>

pkgname=libvolk
pkgver=3.2.0
pkgrel=1
epoch=2
pkgdesc="The Vector-Optimized Library of Kernels from Gnuradio"
arch=(x86_64)
url="https://www.libvolk.org/"
license=(LGPL-3.0-or-later)
depends=(
  gcc-libs
  glibc
  orc
  python
)
makedepends=(
  cmake
  git
  python-mako
)
provides=(libvolk.so)
source=(
  "$pkgname::git+https://github.com/gnuradio/volk.git#tag=v$pkgver"
  "git+https://github.com/google/cpu_features.git"
)
sha256sums=('52bb56c79336650004013b741e9e2dd737a82dcb86219217aa003f4014f06345'
            'SKIP')

prepare() {
  cd $pkgname
  git submodule init
  git config submodule.cpu_features.url "$srcdir/cpu_features"
  git -c protocol.file.allow=always submodule update
}

build() {
  cd $pkgname
  cmake -S . -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=None \
    -Wno-dev
  cmake --build build
}

check() {
  cd $pkgname
  ctest --test-dir build --output-on-failure
}

package() {
  cd $pkgname
  DESTDIR="$pkgdir" cmake --install build
  rm -vr "$pkgdir/usr/include/volk/asm"
}
